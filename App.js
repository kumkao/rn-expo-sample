import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

// Pure Component, not reload on file changed, on state changed only
// export default function App() {
//   return (
//     <View></View>
//   )
// }
// Default Component, always reload on file changed
export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={{color: 'red'}}>Test hot reload</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
