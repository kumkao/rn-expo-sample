import React from 'react'
import { StyleSheet, View, Text, StatusBar } from 'react-native'

export default class MessengerApp extends React.Component {
  render () {
    return (
      <View style={[styles.appContainer, {marginTop: StatusBar.currentHeight}]}>
        <StatusBar hidden={false}/>
        <View style={styles.headerContainer}>
          <Text style={styles.headerText}>Messaging</Text>
        </View>
        <View style={styles.bodyContainer}>
          <Text>No conversations.</Text>
        </View>
        <View style={styles.footerContainer}>
          <View>
            <Text style={[styles.footerText]}>New</Text>
          </View>
          <View>
            <Text style={[styles.footerText]}>Search</Text>
          </View>
          <View>
            <Text style={[styles.footerText]}>More</Text>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  appContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    backgroundColor: '#efefef'
  },
  headerContainer: {
    backgroundColor: '#333',
    paddingLeft: 8,
    paddingTop: 4,
    paddingRight: 8,
    paddingBottom: 4,
  },
  bodyContainer: {
    flexGrow: 1,
    backgroundColor: '#eee',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 8,
    paddingTop: 4,
    paddingRight: 8,
    paddingBottom: 4,
  },
  footerContainer: {
    flexDirection: 'row',
    backgroundColor: '#666',
    justifyContent: 'space-between',
    paddingLeft: 8,
    paddingTop: 4,
    paddingRight: 8,
    paddingBottom: 4,
  },
  headerText: {
    color: '#FFF',
    fontSize: 18
  },
  footerText: {
    color: '#FFF'
  }
})