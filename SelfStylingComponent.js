import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  const myStyle = {
    backgroundColor: '#999'
  }

  return (
    <View style={[styles.container, myStyle]}>
      <Text style={{
        fontWeight: 'bold',
        fontSize: 18
      }}>
        Open up App.js to start working on your app!
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
  },
});