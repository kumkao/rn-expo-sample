import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import PresentationalComponent from './PresentationalComponent'
import RoundButtonStyle from './RoundButtonStyle'

export default class App extends React.Component {
   state = {
      myState: 'This is my state'
   }
   render() {
      return (
         <View>
            <RoundButtonStyle>
               <Button title={Text}>
               </Button>
            </RoundButtonStyle>
         </View>
      )
   }
}

// // self styling comparation
// render() {
//    return (
//       <View>
//          <Button style={styles.roundButton}>Text</Button>
//       </View>
//    )
// }

// const styles = StyleSheet.create({
//    roundButton: {
//      borderRadius: 4,
//      borderWidth: 1
//    }
//  })