import React from 'react'
import { TouchableOpacity, StyleSheet, View, Text } from 'react-native'

// try to replace the TouchableOpacity with another
// - TouchableHighlight
// - TouchableNativeFeedback
// - TouchableWithoutFeedback

const App = () => {
  const handlePress = () => false

  return (
    <View style = {styles.container}>
      <TouchableOpacity onPress={handlePress}>
        <Text style = {styles.text}>
          Button
        </Text>
      </TouchableOpacity>
    </View>
  )
}

export default App

const styles = StyleSheet.create ({
  container: {
    alignItems: 'center',
  },
  text: {
    borderWidth: 1,
    padding: 25,
    borderColor: 'black',
    backgroundColor: 'red'
  }
})