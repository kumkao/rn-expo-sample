import React from 'react'
import { StyleSheet, View } from 'react-native'

export default class RoundButtonStyle extends React.Component {
  render() {
    return (
    <View style={styles.roundButton}>
      {this.props.children}
    </View>
    )
  }
}

const styles = StyleSheet.create({
  roundButton: {
    borderRadius: 4,
    borderWidth: 1
  }
})